SRC_README_MD = """
# Loci Notes `_src` README
This directory is used by [Loci Notes](https://loci-notes.gitlab.io/) to store source code used in code reviews.

You should not need to modify anything in this directory manually. Instead, you should upload source code to the Loci Notes server using the `loci upload` command, and then sync this directory with the server using the `loci sync` command.

Both should be run in the root directory of your project (where the `.loci-project.ini` file and the `_src` directory are).
"""  # noqa: E501
