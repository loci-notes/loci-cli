# Application global vars
LOCI_CONFIG_DIR = "loci"
LOCI_CONFIG_FILENAME = "loci-config.ini"
LOCI_PROJECT_FILENAME = ".loci-project.ini"
LOCI_SRC_DIRECTORY = "_src"

CURRENT_SUPPORTED_SERVER_VERSION = "0.20240219.005324"
# This is a map of Loci server versions and the CLI versions they support. The key is the server version, and the value is a list of CLI versions that support it.
PAST_CLI_SUPPORT_MAP = {"0.14.0": ["0.10.0"]}
LOCI_SERVER_GITLAB_PROJECT_ID = "25489181"
LOCI_CLI_GITLAB_PROJECT_ID = "25202155"
